#immagine di partenza server php-apache 7
FROM php:7.4-apache

#aggiungo i file del portale alla root del server
COPY ./PRISMA_BACKEND /var/www/html

#abilito modulo a2enmod rewrite e installo plugin mysqli
RUN a2enmod rewrite \
    && service apache2 restart \
    && docker-php-ext-install mysqli \
    && docker-php-ext-enable mysqli

