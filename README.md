<p align="center">
  <img src="https://i.imgur.com/IHjGJBk.png" />
</p>

# PRISMA BACKEND
This repository contains the Web-Based backend interface for PRISMA Network.

**install**:
        $ docker build -t prisma/backend -f Dockerfile.build . 

**deploy**:
        $ docker start prisma/backend
