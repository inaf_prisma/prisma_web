/**
*
* @author: N3 S.r.l.
*/

class NodeModel extends Node {
	constructor(){
		super();
	}
	get(id = null,...callBack){
		if (id !== null){
			let endpoint = this.endpointBase + '/'+id;
			getAjax(this,endpoint,...callBack);
		}else{
			let helper = new NodeModel(this.version);
			let obj = this;
			$.each(helper, function (index, value) {
				obj[index] = value;
			});
			callBack.forEach(s => s.apply());
		}
	}
	insert(...callBack){
		let endpoint = this.endpointBase + '';
		let obj = this.parseToObj;
		let json = JSON.stringify(obj);
		postAjax(this,endpoint,json,...callBack);
	}
	update(...callBack){
		let endpoint = this.endpointBase + '';
		let obj = this.parseToObj;
		let json = JSON.stringify(obj);
		putAjax(this,endpoint,json,...callBack);
	}
	erase(id = null, ...callBack){
		let endpoint = this.endpointBase + '';
		let helper = new NodeModel(this.version);
		helper.endpoint = this.endpoint;
		helper.id = id;
		let obj = helper.parseToObj;
		let json = JSON.stringify(obj);
		patchAjax(this,endpoint,json,...callBack);
	}
	delete(id = null, ...callBack){
		let endpoint = this.endpointBase + '';
		let helper = new NodeModel(this.version);
		helper.endpoint = this.endpoint;
		helper.id = id;
		let obj = helper.parseToObj;
		let json = JSON.stringify(obj);
		deleteAjax(this,endpoint,json,...callBack);
	}
	get parseToObj(){
		let obj = {
			id: this.id,
			oid: this.oid,
			station_id: this.station_id,
			MAC_address: this.MAC_address,
			hostname: this.hostname,
			CName: this.CName,
			freeture_configuration_file: this.freeture_configuration_file,
			ovpnfile: this.ovpnfile,
			interval_running_DEA: this.interval_running_DEA,
			relative_path: this.relative_path,
		};
		return obj;
	}
	parseJsonToObj(context_,json,...callBack){
		let obj_full = JSON.parse(json);
		let obj = obj_full.data;
		let context = context_;
		context.id = obj.id;
		context.oid = obj.oid;
		context.station_id = obj.station_id;
		context.MAC_address = obj.MAC_address;
		context.hostname = obj.hostname;
		context.CName = obj.CName;
		context.freeture_configuration_file = obj.freeture_configuration_file;
		context.ovpnfile = obj.ovpnfile;
		context.interval_running_DEA = obj.interval_running_DEA;
		context.relative_path = obj.relative_path;
		//callback
		callBack.forEach(s => s.apply());
	}
}
function setNodeVisibility(){ 

	let prnode_visibility = new Node(); 

	prnode_visibility.id = true; 
	prnode_visibility.oid = true; 
	prnode_visibility.station_id = true; 
	prnode_visibility.MAC_address = true; 
	prnode_visibility.hostname = true; 
	prnode_visibility.CName = true; 
	prnode_visibility.freeture_configuration_file = true; 
	prnode_visibility.ovpnfile = true; 
	prnode_visibility.interval_running_DEA = true; 
	prnode_visibility.relative_path = true; 
	prnode_visibility.modified_by = true; 
	prnode_visibility.created_by = true; 
	prnode_visibility.assigned = true; 
	prnode_visibility.erased = true; 
	prnode_visibility.last_update = true; 
	

	$.each(prnode_visibility, function(key, value){
		if (!value)
			$("." +$.md5(key)).hide();
	});
}

