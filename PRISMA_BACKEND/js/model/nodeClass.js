/**
*
* @author: N3 S.r.l.
*/

class Node extends N3Obj{
	constructor(){
		super("1","prismaCore","node");
		this.station_id = null;
		this.MAC_address = null;
		this.hostname = null;
		this.CName = null;
		this.freeture_configuration_file = null;
		this.ovpnfile = null;
		this.interval_running_DEA = null;
		this.relative_path = null;
	}
}


