/**
*
* @author: N3 S.r.l.
*/

$(setDetectionVisibility());
var prdetection = new DetectionModel('1');
var lastEditId = '';
var indexToShow = null;
$(function(){
	disableForm(prdetection);

});

function setLastEditId(){
	lastEditId = prdetection.id;
}

function editObj(id){
	disableForm(prdetection,true);
	detectionLogic.get(prdetection,id,function (){
            $("#GeMapImg").attr('src', prdetection.GeMapImg);
            $("#DirMapImg").attr('src',  prdetection.DirMapImg);
            $("#EventImg").attr('src',  prdetection.EventImg);
        });
	$('td').removeClass('lastEditedRow');
	lastEditId = '';
}

function allowEditObj(){
	enableForm(prdetection,false);
}

function saveObj(){
	var f = function(){disableForm(prdetection);}
	detectionLogic.save(prdetection,setIndexToShow,setLastEditId, f, reloadAllDatatable);
}

function removeObj(){
	var f = function(){disableForm(prdetection);}
	detectionLogic.remove(prdetection,prdetection.id, safeDelete, f, reloadAllDatatable);
}

function newObj(){
	newForm(prdetection);
	detectionLogic.get(prdetection, null);
	$('td').removeClass('lastEditedRow');
	lastEditId = '';
}

function undoObj(){
	var f = function(){
		editObj(prdetection.id);
	};
	alertConfirm("Conferma", "Sei sicuro di voler annullare le modifiche? Le modifiche non salvate andranno perse", f);
}

function setIndexToShow(){
	indexToShow = prdetection.id;
}

$(document).ready(function () {
	table = $('#DetectionList').DataTable({
		"oLanguage": {
			"sZeroRecords": "Nessun risultato",
			"sSearch": "Cerca:",
			"oPaginate": {
				"sPrevious": "Indietro",
				"sNext": "Avanti"
			},
			"sInfo": "Mostra pagina _PAGE_ di _PAGES_",
			"sInfoFiltered": "",
			"sInfoEmpty": "Mostra pagina 0 di 0 elementi",
			"sEmptyTable": "Nessun risultato",
			"sLengthMenu": "Mostra _MENU_ elementi"
			},
		"columnDefs": [{
				"targets": [-2 ],
				"orderable": false
			},
			{
				"targets": [-1],
				"visible": false
			}],
		responsive: true,
		dom: 'lfrt<t>ip',
		"fnServerParams": function (aoData) {
			// Show page with passed index
			aoData.push({"name": "searchPageById", "value": indexToShow});
			if ($("." +$.md5('id')).is(":visible"))
				aoData.push({"name": "id", "value": $('#F_id').val()});
			if ($("." +$.md5('oid')).is(":visible"))
				aoData.push({"name": "oid", "value": $('#F_oid').val()});
			if ($("." +$.md5('node_id')).is(":visible"))
				aoData.push({"name": "node_id", "value": $('#F_node_id').val()});
			if ($("." +$.md5('event_id')).is(":visible"))
				aoData.push({"name": "event_id", "value": $('#F_event_id').val()});
			if ($("." +$.md5('inserted_timestamp')).is(":visible"))
				aoData.push({"name": "inserted_timestamp", "value": $('#F_inserted_timestamp').val()});
			if ($("." +$.md5('detected_timestamp')).is(":visible"))
				aoData.push({"name": "detected_timestamp", "value": $('#F_detected_timestamp').val()});
			if ($("." +$.md5('is_fake')).is(":visible"))
				aoData.push({"name": "is_fake", "value": $('#F_is_fake').val()});
		},
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			if (aData[aData.length - 1] == lastEditId) {
				$('td', nRow).addClass('lastEditedRow');
			}
		},
		"fnDrawCallback": function (settings, json) {
			// Show page with passed index
			indexToShow = null;
				setTimeout(function () {
				if (settings.json.pageToShow !== null) {
					if ($('.dataTable').DataTable().page.info().page !== settings.json.pageToShow) {
						$('.dataTable').DataTable().page(settings.json.pageToShow).draw('page');
					}
				}
			}, 100);
		},
		bProcessing: true,
		bServerSide: true,
		bStateSave: true,
		sAjaxSource: '/lib/prismaCore/1/detection/datatable/list'
	});
});

 $(function() {
initFilters();
});
	var setData = {
		singleDatePicker: true, opens: 'right',
		calender_style: "picker_2",
		format: 'DD/MM/YYYY'
	};
function initFilters() {
	$(".filter-text").each(function (index) {
		$(this).select2({
			language: 'it',
			maximumSelectionLength: 1,
			multiple: true,
			ajax: {
				url: '/lib/prismaCore/1/detection/autocomplete/' + $(this).attr('id').replace('F_',''),
				dataType: 'json'
			},
			minimumInputLength: 1
		});
	});
	$(".filter-date, .date").each(function (index) {
		$(this).daterangepicker(setData, function(){reloadAllDatatable();});
	});
	$(".foreign_key").each(function (index) {
		$(this).select2({
			language: 'it',
			maximumSelectionLength: 0,
			multiple: false,
			ajax: {
				url: '/lib/prismaCore/1/detection/foreignkey/' + $(this).attr('id').replace('F_',''),
				dataType: 'json'
			},
			minimumInputLength: 0
		});
	});
}

