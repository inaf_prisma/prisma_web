<?php
/**
 * Class for Node
 * 
 * @author: N3 S.r.l.
 */
class Node extends N3BusinessObject
{
	public $station_id;
	public $MAC_address=null;
	public $hostname=null;
	public $CName;
	public $freeture_configuration_file=null;
	public $ovpnfile=null;
	public $interval_running_DEA=null;
	public $relative_path;
}
?>
