<?php /* @var $Node Node */ ?> 
<div class='right_col' role='main'>
	<div class=''>
		<div class='page-title'>
			<div class='title_left'>
				<h2><?= _('Node') ?></h2>
			</div>
		</div>
			<div class='clearfix'></div>
				<div class='row'>
					<div class='col-md-12 col-sm-12 col-xs-12'>
					<div id='list' class='x_panel'>
						<div class='x_title no-padding-lr'>
							<div class='clearfix'>
								<div class='col-md-6 no-padding-l'>
									<h2><?= _('Elenco') ?></h2>
								</div>
									<div class='col-md-6 no-padding'>
										<a href = '#edit' ><button type='button' onclick='newObj()' style='margin-right: 10px' class='btn btn-success pull-right' ><?= _('AGGIUNGI NUOVO NODO') ?></button></a>
									</div>
								</div>
							</div>
							<div class='x_panel filter-container'>
								<div class='x_title filter-title-container collapse-link'>
									<div class='filter-title'>
										<h2 class='font-15'>Filtra per...</h2>
										<ul class='nav navbar-right panel_toolbox'>
											<li><a class='black'><i class='fa fa-chevron-down'></i></a>
											</li>
										</ul>
									</div>
								<div class='clearfix'></div>
							</div>
							<div class='x_content filter-content' hidden>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 b80bb7740288fda1f201890375a60c8f'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('id')) ?></small>
											<select class='form-control filter filter-text' id='F_id' multiple='multiple' title='<?php echo (_('Filtra per id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 130f43112bb8a7a7790ebfc08ee9d6af'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('oid')) ?></small>
											<select class='form-control filter filter-text' id='F_oid' multiple='multiple' title='<?php echo (_('Filtra per oid')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 c04dd157d66144e6e8413fa4d49aefaa'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('station_id')) ?></small>
											<select class='form-control filter filter-text' id='F_station_id' multiple='multiple' title='<?php echo (_('Filtra per station_id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 5061aeeac05c2c0d81dee47fccc9de9b'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('MAC_address')) ?></small>
											<select class='form-control filter filter-text' id='F_MAC_address' multiple='multiple' title='<?php echo (_('Filtra per MAC_address')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 0897acf49c7c1ea9f76efe59187aa046'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('hostname')) ?></small>
											<select class='form-control filter filter-text' id='F_hostname' multiple='multiple' title='<?php echo (_('Filtra per hostname')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 398ccf8cf34c72fdae6ac112568abd2d'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('CName')) ?></small>
											<select class='form-control filter filter-text' id='F_CName' multiple='multiple' title='<?php echo (_('Filtra per CName')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 21c79fb0900168bf1e68b3ca7f2728cc'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('freeture_configuration_file')) ?></small>
											<select class='form-control filter filter-text' id='F_freeture_configuration_file' multiple='multiple' title='<?php echo (_('Filtra per freeture_configuration_file')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 fa31516abd4b0447bd93ad55ea5304ca'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('ovpnfile')) ?></small>
											<select class='form-control filter filter-text' id='F_ovpnfile' multiple='multiple' title='<?php echo (_('Filtra per ovpnfile')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 9b81faff95d4d4709d4951784f8b57f0'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('interval_running_DEA')) ?></small>
											<select class='form-control filter filter-text' id='F_interval_running_DEA' multiple='multiple' title='<?php echo (_('Filtra per interval_running_DEA')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 14b0aee5bb3e06cfa8def07f61fe47cf'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('relative_path')) ?></small>
											<select class='form-control filter filter-text' id='F_relative_path' multiple='multiple' title='<?php echo (_('Filtra per relative_path')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-12 col-sm-12 col-xs-12'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<button class="pull-right btn btn-success applyFilter" ><?= _("Applica filtri") ?></button>
										</div>
									</div>
								</div>
							</div>
						</div>
							<div class='x_content'>
								<table id='NodeList' class='table table-striped table-bordered' style='width: 100%; '>
									<thead>
									<tr>
										<th><?php echo (_('station_id')) ?></th>
										<th><?php echo (_('MAC_address')) ?></th>
										<th><?php echo (_('interval_running_DEA')) ?></th>
										<th><?php echo (_('relative_path')) ?></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class='col-md-12 col-sm-12 col-xs-12'>
					<form id='CompanyForm' method='POST' action='/service/node/save/<?php echo $Node->id;?>' class='form-horizontal form-label-left' novalidate>
						<div id='edit' class='x_panel'>
							<div class='x_title no-padding-lr'>
								<div class='clearfix'>
									<div class='col-md-8 no-padding'>
										<h2><?= _('Aggiungi nuovo') ?></h2>
									</div>
									<div class='col-md-4 no-padding'>
										<button type = 'button' style= 'display: none; margin-right: 10px;' id= 'deletebtn' class='btn btn-danger pull-right' onclick="removeObj()" ><?= _('ELIMINA') ?></button>
										<button type = 'submit' style= 'display: none; margin-right: 10px;' id= 'savebtn' class='btn btn-success pull-right' ><?= _('SALVA') ?></button>
										<button type = 'button' style='display: none; margin-right: 10px;' id='cleanbtn' onclick='newObj()' class='btn btn-clean pull-right cleanForm' ><?= _('PULISCI CAMPI') ?></button>
										<button type = 'button' style= 'display: none; margin-right: 10px;' id= 'modifybtn' onclick= 'allowEditObj();' class='btn btn-success btn-blue-success pull-right' ><?= _('MODIFICA') ?></button>
										<button type = 'button' style= 'display: none; margin-right: 10px;' id= 'undobtn' onclick= 'undoObj();' class='btn btn-warning btn-yellow-warning pull-right' ><?= _('ANNULLA') ?></button>
										<a href = '#list' ><button type='button' style='margin-right: 10px' class='btn btn-all pull-right' ><?= _('TUTTI') ?></button></a>
									</div>
								</div>
							</div>
							<div class='x_content'>
								<div class='col-md-12 col-sm-12 col-xs-12'>
									<div class='item form-group'>
										<label class='col-md-6 col-sm-6 col-xs-12' ><?= _('I campi contrassegnati con * sono obbligatori') ?></label>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 b80bb7740288fda1f201890375a60c8f'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('id')) ?> </small>
											<input type = 'text' name='id' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('id')) ?> '  title=' <?php echo ( _('id')) ?> '/>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 130f43112bb8a7a7790ebfc08ee9d6af'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('oid')) ?> </small>
											<input type = 'text' name='oid' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('oid')) ?> '  title=' <?php echo ( _('oid')) ?>' maxlength = '32' />
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 c04dd157d66144e6e8413fa4d49aefaa'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('station_id')) ?> <span class='required'>*</span></small>
											<select class='form-control foreign_key' id='station_id' name='station_id' multiple='multiple' required='required'  title='<?php echo(_('station_id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 5061aeeac05c2c0d81dee47fccc9de9b'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('MAC_address')) ?> </small>
											<input type = 'text' name='MAC_address' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('MAC_address')) ?> '  title=' <?php echo ( _('MAC_address')) ?>' maxlength = '45' />
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 0897acf49c7c1ea9f76efe59187aa046'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('hostname')) ?> </small>
											<input type = 'text' name='hostname' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('hostname')) ?> '  title=' <?php echo ( _('hostname')) ?>' maxlength = '100' />
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 398ccf8cf34c72fdae6ac112568abd2d'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('CName')) ?> <span class='required'>*</span></small>
											<input type = 'text' name='CName' required='required' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('CName')) ?> '  title=' <?php echo ( _('CName')) ?>' maxlength = '100' />
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 21c79fb0900168bf1e68b3ca7f2728cc'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('freeture_configuration_file')) ?> </small>
											<input type = 'text' name='freeture_configuration_file' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('freeture_configuration_file')) ?> '  title=' <?php echo ( _('freeture_configuration_file')) ?>' maxlength = '100' />
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 fa31516abd4b0447bd93ad55ea5304ca'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('ovpnfile')) ?> </small>
											<input type = 'text' name='ovpnfile' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('ovpnfile')) ?> '  title=' <?php echo ( _('ovpnfile')) ?>' maxlength = '100' />
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 9b81faff95d4d4709d4951784f8b57f0'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('interval_running_DEA')) ?> </small>
											<input type = 'text' name='interval_running_DEA' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('interval_running_DEA')) ?> '  title=' <?php echo ( _('interval_running_DEA')) ?> '/>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 14b0aee5bb3e06cfa8def07f61fe47cf'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('relative_path')) ?> <span class='required'>*</span></small>
											<input type = 'text' name='relative_path' required='required' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('relative_path')) ?> '  title=' <?php echo ( _('relative_path')) ?>' maxlength = '100' />
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php include './view/template/foot.php'; ?>
	<script src='<?php echo $_SERVER['PATH_WEBROOT'] ?>/js/node.js<?= _VERSION_ ?>'></script>

