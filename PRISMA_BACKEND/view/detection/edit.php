<?php /* @var $Detection Detection */ ?> 
<div class='right_col' role='main'>
	<div class=''>
		<div class='page-title'>
			<div class='title_left'>
				<h2><?= _('Detection') ?></h2>
			</div>
		</div>
			<div class='clearfix'></div>
				<div class='row'>
					<div class='col-md-12 col-sm-12 col-xs-12'>
					<div id='list' class='x_panel'>
						<div class='x_title no-padding-lr'>
							<div class='clearfix'>
								<div class='col-md-6 no-padding-l'>
									<h2><?= _('Elenco') ?></h2>
								</div>
									
								</div>
							</div>
							<div class='x_panel filter-container'>
								<div class='x_title filter-title-container collapse-link'>
									<div class='filter-title'>
										<h2 class='font-15'>Filtra per...</h2>
										<ul class='nav navbar-right panel_toolbox'>
											<li><a class='black'><i class='fa fa-chevron-down'></i></a>
											</li>
										</ul>
									</div>
								<div class='clearfix'></div>
							</div>
							<div class='x_content filter-content' hidden>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 b80bb7740288fda1f201890375a60c8f'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('id')) ?></small>
											<select class='form-control filter filter-text' id='F_id' multiple='multiple' title='<?php echo (_('Filtra per id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 130f43112bb8a7a7790ebfc08ee9d6af'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('oid')) ?></small>
											<select class='form-control filter filter-text' id='F_oid' multiple='multiple' title='<?php echo (_('Filtra per oid')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 c693ebc80e2b73b0d3bbece47c529399'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('node_id')) ?></small>
											<select class='form-control filter filter-text' id='F_node_id' multiple='multiple' title='<?php echo (_('Filtra per node_id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-3 col-sm-6 col-xs-12 4437cfaca07febc397b36b521305be41'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('event_id')) ?></small>
											<select class='form-control filter filter-text' id='F_event_id' multiple='multiple' title='<?php echo (_('Filtra per event_id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 f3919f71d2ac66ddfbcd8217e2ddbcc3'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('inserted_timestamp')) ?></small>
											<input type='text' autocomplete='off' class='form-control filter filter-date' id='F_inserted_timestamp' title='<?php echo (_('Filtra per inserted_timestamp')) ?>'>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 8957504e807357f35561f6f5b9778563'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('detected_timestamp')) ?></small>
											<input type='text' autocomplete='off' class='form-control filter filter-date' id='F_detected_timestamp' title='<?php echo (_('Filtra per detected_timestamp')) ?>'>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 44fb223af34728398d243db75b59bcde'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo (_('is_fake')) ?></small>
											<select class='form-control filter filter-checkbox' id='F_is_fake' title='<?php echo (_('Filtra per is_fake')) ?>'>
												<option value=''></option>
												<option value='1'> <?php echo (_('Sì')) ?></option>
												<option value='0'> <?php echo (_('No')) ?></option>
											</select>
										</div>
									</div>
								</div>
								<div class='form-group col-md-12 col-sm-12 col-xs-12'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<button class="pull-right btn btn-success applyFilter" ><?= _("Applica filtri") ?></button>
										</div>
									</div>
								</div>
							</div>
						</div>
							<div class='x_content'>
								<table id='DetectionList' class='table table-striped table-bordered' style='width: 100%; '>
									<thead>
									<tr>
										<th><?php echo (_('oid')) ?></th>
										<th><?php echo (_('node_id')) ?></th>
										<th><?php echo (_('event_id')) ?></th>
										<th><?php echo (_('inserted_timestamp')) ?></th>
										<th><?php echo (_('detected_timestamp')) ?></th>
										<th><?php echo (_('is_fake')) ?></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class='col-md-12 col-sm-12 col-xs-12'>
					<form id='CompanyForm' method='POST' action='/service/detection/save/<?php echo $Detection->id;?>' class='form-horizontal form-label-left' novalidate>
						<div id='edit' class='x_panel'>
							<div class='x_title no-padding-lr'>
								<div class='clearfix'>
									<div class='col-md-8 no-padding'>
										<h2><?= _('Aggiungi nuovo') ?></h2>
									</div>
									<div class='col-md-4 no-padding'>
										<button type = 'button' style= 'display: none; margin-right: 10px;' id= 'deletebtn' class='btn btn-danger pull-right' onclick="removeObj()" ><?= _('ELIMINA') ?></button>
										<button type = 'submit' style= 'display: none; margin-right: 10px;' id= 'savebtn' class='btn btn-success pull-right' ><?= _('SALVA') ?></button>
										<button type = 'button' style='display: none; margin-right: 10px;' id='cleanbtn' onclick='newObj()' class='btn btn-clean pull-right cleanForm' ><?= _('PULISCI CAMPI') ?></button>
										<button type = 'button' style= 'display: none; margin-right: 10px;' id= 'modifybtn' onclick= 'allowEditObj();' class='btn btn-success btn-blue-success pull-right' ><?= _('MODIFICA') ?></button>
										<button type = 'button' style= 'display: none; margin-right: 10px;' id= 'undobtn' onclick= 'undoObj();' class='btn btn-warning btn-yellow-warning pull-right' ><?= _('ANNULLA') ?></button>
										<a href = '#list' ><button type='button' style='margin-right: 10px' class='btn btn-all pull-right' ><?= _('TUTTI') ?></button></a>
									</div>
								</div>
							</div>
							<div class='x_content'>
								<div class='col-md-12 col-sm-12 col-xs-12'>
									<div class='item form-group'>
										<label class='col-md-6 col-sm-6 col-xs-12' ><?= _('I campi contrassegnati con * sono obbligatori') ?></label>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 b80bb7740288fda1f201890375a60c8f'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('id')) ?> </small>
											<input type = 'text' name='id' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('id')) ?> '  title=' <?php echo ( _('id')) ?> '/>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 130f43112bb8a7a7790ebfc08ee9d6af'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('oid')) ?> </small>
											<input type = 'text' name='oid' class='form-control col-md-7 col-xs-12 input-disabled' placeholder=' <?php echo ( _('oid')) ?> '  title=' <?php echo ( _('oid')) ?>' maxlength = '32' />
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 c693ebc80e2b73b0d3bbece47c529399'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('node_id')) ?> <span class='required'>*</span></small>
											<select class='form-control foreign_key' id='node_id' name='node_id' multiple='multiple' required='required'  title='<?php echo(_('node_id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 4437cfaca07febc397b36b521305be41'>
									<div class='form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('event_id')) ?></small>
											<select class='form-control foreign_key' id='event_id' name='event_id' title='<?php echo(_('event_id')) ?>'>
											</select>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 f3919f71d2ac66ddfbcd8217e2ddbcc3'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('inserted_timestamp')) ?> </small>
											<input type = 'text' autocomplete='off' name='inserted_timestamp' class='form-control date col-md-7 col-xs-12 has-feedback-left input-disabled' date='date' placeholder='<?php echo ( _('inserted_timestamp')) ?>' title=<?php echo ( _('inserted_timestamp')) ?>/>
											<span class='fa fa-calendar-o form-control-feedback left' aria-hidden='true'></span>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 8957504e807357f35561f6f5b9778563'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<small class='text-muted'><?php echo ( _('detected_timestamp')) ?> </small>
											<input type = 'text' autocomplete='off' name='detected_timestamp' class='form-control date col-md-7 col-xs-12 has-feedback-left input-disabled' date='date' placeholder='<?php echo ( _('detected_timestamp')) ?>' title=<?php echo ( _('detected_timestamp')) ?>/>
											<span class='fa fa-calendar-o form-control-feedback left' aria-hidden='true'></span>
										</div>
									</div>
								</div>
								<div class='col-md-3 col-sm-6 col-xs-12 44fb223af34728398d243db75b59bcde'>
									<div class='item form-group'>
										<div class='col-xs-12'>
											<label class='text-muted checkbox-label'><?php echo ( _('is_fake')) ?></label>
											<input type = 'checkbox' onclick="$(this).val(this.checked ? 1 : 0)"  name='is_fake' class='col-md-1 col-xs-1 checkbox input-disabled' placeholder='<?php echo ( _('is_fake')) ?>' title='<?php echo ( _('is_fake')) ?>'>
										</div>
									</div>
								</div>
                                                                <div class='col-md-3 col-sm-6 col-xs-12 44fb223af34728398d243db75b59bcde'>
                                                                    <img id="GeMapImg" src="" alt="GeMap" height="240">
                                                                    <img id="DirMapImg" src="" alt="DirMap" height="240">
                                                                    <img id="EventImg" src="" alt="Event Preview" height="240" > 
                                                                </div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php include './view/template/foot.php'; ?>
	<script src='<?php echo $_SERVER['PATH_WEBROOT'] ?>/js/detection.js<?= _VERSION_ ?>'></script>

